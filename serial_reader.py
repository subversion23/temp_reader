import serial
import time
import os
import datetime
import io
import threading
import queue
import random
from  pyhelper.helper import log,PSQLConnector
# first out: b'\x00\n\n[LaCrosseITPlusReader.10.1s (RFM69CW f:868300 r:17241)]\r\nOK 9 23 1 4 222 47\r\n'
# output: b'OK 9 23 1 4 222 47\r\n'


env = os.name
upd_time_sec = 5

if env == 'posix':
    DEBUG = False
else:
    DEBUG = True


class Pseudo_Serial():
    i = 0
    def readall(self):
        return b'OK 9 23 1 4 222 47\r\nOK 9 23 1 4 222 47\r\n'
    def readline(self):
        return 'OK 12 2 1 4 222 {0}\r\n'.format(random.randint(0,1))
    def flushOutput(self):
        pass
    def readlines(self):
        return b'OK 9 23 1 4 222 47\r\nOK 9 23 1 4 222 47\r\n'


if env == 'posix':
    serialport = serial.Serial("/dev/ttyUSB0", 57600, timeout=1) #17240 gives bytes not str!!57600
else:
    serialport = Pseudo_Serial()

data1 =    b'OK 9 23 1 4 222 47\r\n' #=24.6C
data2 = b'OK 9 23 1 5 14 53\r\n' #~29C
data3 =  b'OK 9 23 1 5 33 50\r\n' #31.4
bdata1 = b'\xd8\x0c\x90L\xf9' # 29.5C 54H

def parse_temp_hum_bytes(data):
    if DEBUG:
        log(data) #<- DEBUG!

def parse_temp_hum(data):
    error_retcode = 0#(-99,0,"")
    #data = str(data).strip("\r\n")
    #if DEBUG:
    log(data) #<- DEBUG!
    ldata = data.split()
    if len(ldata) != 7:
        log("Corrupt Data"+os.linesep+str(data))
        return error_retcode
    #TODO check if corrupt - regex?

    try:
        bat = ldata[0]
        temp_a = int(ldata[4])
        temp_b = int(ldata[5])
        if ldata[2] != b'23':
            log("Not my Device"+os.linesep+str(data))
            return error_retcode

    except ValueError:
        log("error while temp_hum parse"+os.linesep+str(data))
        return error_retcode

    hum = int(ldata[6])
    #print (temp_a,temp_b)

    ab = "{0:04b}{1:08b}".format(temp_a,temp_b)  #only first 4 bit of a
    temp = (int(ab,2) -1000 )/10
    #print (temp,hum,bat) #DEBUG!
    #debug:
    #if temp < 20 or temp > 40:
    #    print (data)
    #    print (temp,hum,bat)

    return temp,hum,bat

class SerialListener(threading.Thread):
    def __init__(self,event,exbucket=None,group=None, target=None, name=None,verbose=None):
        #super(SerialListener,self).__init__(group=group, target=target,
        #        			             name=name, verbose=verbose)
        # Which one?
        super(SerialListener,self).__init__()
        self.exbucket = exbucket
        self.bRun = True
        self.lastdata = ()
        self.event = event
        self.Data = ()

    # Override
    def run(self):
        self.Main()

    def _read_data(self):
        data = []
        try:
            data = serialport.readlines()
        except Exception as ex:
            log(str(ex))
        #print (data)
        #print (data[len(data)-1:])
        if data == []:
            return
        ret =  parse_temp_hum(data[len(data)-1:][0]) #get last line

        #serialport.flushOutput()
        if type(ret) is tuple:
            return ret

    def Main(self):
        while self.bRun: # Run until dead, all ex out
            try:
                curdata = self._read_data()
                if curdata:
                    if self.lastdata != curdata:
                        self.Data = curdata
                        self.event.set()
                    self.lastdata = curdata
            except Exception as ex:
                self.exbucket.put(ex)

            if DEBUG:
                time.sleep (10) # TEST
            else:
                time.sleep(90)

    def Stop(self):
        self.bRun = False
        self.join(0.1)
        log ("Called Stop and joined")

bucket = queue.Queue()
event = threading.Event()
DB = PSQLConnector()
conn = DB.open("wu",host="archimedes")


def write_to_db(temp=None,hum=None):
    #.strftime("%d.%m.%Y - %H:%M:%f")
    now = datetime.datetime.now()
    cur =conn.cursor()
    if DEBUG:
        schema = "fhem"
    else:
        schema = "homedata"
    if temp:
        query = 'insert into "{0}"."lc17_temp" VALUES(%s,%s)'.format(schema)
        data = (now,temp)
        cur.execute(query,data)

    if hum:
        query = 'insert into "{0}"."lc17_hum" VALUES(%s,%s)'.format(schema)
        data = (now,hum)
        cur.execute(query,data)

    cur.close()
    conn.commit()

class ValueProcessor:
    def __init__(self):
        self.last_temp = 0
        self.last_hum = 0

    def process_values(self,data):
        temp = data[0]
        hum = data[1]
        if self.last_temp != temp and self.last_hum != hum:
            write_to_db(temp,hum)
        else:
            if self.last_temp != temp:
                write_to_db(temp=temp)
            if self.last_hum != hum:
                write_to_db(hum=hum)
        self.last_hum = hum
        self.last_temp= temp


VP = ValueProcessor()

SL = SerialListener(event,bucket)
SL.start()
bRun = True
log("Gestartet!")

waiter_timeout = 5 #lower cause exeption check


def Run():
    while bRun:
        try:
            ex = bucket.get(block=False)
            log("ex in main try:" + str(ex.args))
            raise ex
            #log(ex.args[0])
        except queue.Empty: # if no exceptions: Main
            if event.wait(waiter_timeout):
                data = SL.Data
                event.clear()
                # lock clear -> process
                log(data)
                VP.process_values(data)
            else:
                pass
                #log("No Data after" + str(waiter_timeout))
        else:
            pass
            #raise ex
try:
    Run()
except KeyboardInterrupt:
    SL.Stop()
    DB.close()
    bRun = False
    log("th1 stopped\nEND")

#print(str(threading.current_thread()))


